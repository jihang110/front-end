import axios from 'axios';
import packjson from '../../package.json';

let util = {};
// util.title = function(title = '') {
//     window.document.title = 'jihangTest - ' + title;
// };

util.ajax = axios.create({
    baseURL: packjson.ws.host,
    timeout: 30000
});

util.isEmptyObject = (obj) => {
    var name;
    for (name in obj) {
        return false;
    }
    return true;
}

export default util;
