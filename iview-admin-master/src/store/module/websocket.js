import store from './user'
const state = {
  websock: null
}

const mutations = {
  STAFF_UPDATEWEBSOCKET (state, websock) {
    state.websock = websock
  }
  // STAFF_SEND (state, text) {
  //   state.websock.send(text)
  // }
}

// 实现websocket的连接，需要携带参数token
const actions = {
  // 用到 ES2015 的参数解构来简化代码（特别是我们需要调用 commit 很多次的时候）
  STAFF_WEBSOCKET ({ commit },res) {
    let token = encodeURI('Bearer ' + store.state.token)
    // const wsuri = 'wss://XXXXXXXXX/?Authorization=' + token + '&EIO=3&transport=websocket'
    const wsuri = 'ws://localhost:9300/ws?userName='+res.username;
    commit('STAFF_UPDATEWEBSOCKET', new WebSocket(wsuri))
    // 只有定义了onopen方法，才能继续实现接收消息，即在使用的地方调用onmessage方法。
    state.websock.onopen = function () {
      console.log("socket连接成功")
    }
    state.websock.onclose = function () {
      console.log("socket连接断开")
    }
    state.websock.onclose = function () {
      console.log("socket连接断开")
    }
    state.websock.error = function () {
      console.log("连接错误")
    }
    // 心跳包，30s左右无数据浏览器会断开连接Heartbeat
    // setInterval(function () {
    //   state.websock.send(JSON.stringify({
    //     'heart': true
    //   }))
    // }, 30000)
  }
}

// 该部分为了获取websocket的相关方法。会发现此处跟mutations 里的写法是类似的，但是，想使用return，需要将相关数据写在getters里面。
const getters = {
  STAFF_UPDATE (state) {
    return state.websock
  }
}
export default {
  state,
  mutations,
  actions,
  getters
}
