import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Index from '@/components/index.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      redirect: '/item1/1-1',
      component: Index
    }
    ,{
      path:'/item1',
      component: Index,
      redirect: '/item1/1-1',
      name:'item1',
      meta: {
        title: 'item1'
      },
      children:[
        {
          path: '1-1',
          meta: {
            title: '1-1'
          },
          name: '1-1',
          component: (resolve) => require(['@/components/test/test1-1.vue'], resolve)
        },
        {
          path: '1-2',
          meta: {
            title: '1-2'
          },
          name: '1-2',
          component: (resolve) => require(['@/components/test/test1-2.vue'], resolve)
        },
        {
          path: '1-3',
          meta: {
            title: '1-3'
          },
          name: '1-3',
          component: (resolve) => require(['@/components/test/test1-3.vue'], resolve)
        }
      ]
    } ,{
      path:'/item2',
      component: Index,
      redirect: '/item2/2-1',
      meta: {
        title: 'item2'
      },
      name:'item2',
      children:[
        {
          path: '2-1',
          meta: {
            title: '2-1'
          },
          name: '2-1',
          component: (resolve) => require(['@/components/test/test2-1.vue'], resolve)
        },
        {
          path: '2-2',
          meta: {
            title: '2-2'
          },
          name: '2-2',
          component: (resolve) => require(['@/components/test/test2-2.vue'], resolve)
        }
      ]
    },{
      path:'/item3',
      redirect: '/item3/3-1',
      component: Index,
      meta: {
        title: 'item3'
      },
      name:'item3',
      children:[
        {
          path: '3-1',
          meta: {
            title: '3-1'
          },
          name: '3-1',
          component: (resolve) => require(['@/components/test/test3-1.vue'], resolve)
        },
        {
          path: '3-2',
          meta: {
            title: '3-2'
          },
          name: '3-2',
          component: (resolve) => require(['@/components/test/test3-2.vue'], resolve)
        }
      ]
    }
  ]
})
